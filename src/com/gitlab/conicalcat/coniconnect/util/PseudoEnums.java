package com.gitlab.conicalcat.coniconnect.util;

/**
 * @author ConicalCat
 * An enum of Pseudo-enums.
 */
public enum PseudoEnums {
    CONNECTIONERRORS((byte) 0x01, "ConnectionErrors"), CONNECTIONSTATUS((byte) 0x02, "");
    
    byte ordinal;
    String name;
    
    private PseudoEnums(byte ordinal, String name) {
        
    }
    
    private abstract class BaseOfPseudoEnums {
        public byte ordinal;
        public String name;
        public byte getOrdinal() {
            return ordinal;
        }
        public String getName() {
            return name;
        }
        public BaseOfPseudoEnums(byte ordinal, String name) {
            this.ordinal = ordinal;
            this.name = name;
        }
        abstract BaseOfPseudoEnums fromCode(byte code);
    }
    
    /**
     * An enum of states. each complete with Javadoc.
     * For my own convenience's sake, connection do not throw unique exceptions,
     * save for if the developer using the library makes a stupid mistake, like
     * trying to connect through a port above 65535, which, just as an example,
     * throws {@link
     * com.gitlab.conicalcat.coniconnect.exception.InvaliPortException an <code>
     * InvalidPortException} </code>
     * @see com.gitlab.conicalcat.coniconnect.util.Connection
     */
    public static class ConnectionError extends BaseOfPseudoEnums {
        /**
         * No error. Connection was successful.
         * {@link com.gitlab.conicalcat.coniconnect.util.Connection#getFailed()
         * <ordinal>getFailed()</ordinal>} must have been called mistakenly.
         */ 
        public static final ConnectionError OK = new ConnectionError((byte) 0, "No error.");
         /** Couldn't resolve hostname */
        public static final ConnectionError UNRESOLVED = new ConnectionError((byte) 1, "Couldn't resolve hostname.");
         /** Connection refused */
        public static final ConnectionError REFUSED = new ConnectionError((byte) 2, "Connection refused");
        /** Server timed out */
        public static final ConnectionError TIMEOUT = new ConnectionError((byte) 3, "Server timed out.");
     
        public static final byte enumCode = 0x01;
        public static final String enumName = "ConnectionErrors";
        
        public String name;
        public byte ordinal;
        
        private ConnectionError(byte ordinal, String name) {
            super(ordinal, name);
            this.name = name;
            this.ordinal = ordinal;
        }
        
        public ConnectionError fromCode(byte ordinal) {
            if (ordinal == OK.ordinal) {
                return OK;
            } else if (ordinal == UNRESOLVED.ordinal) {
                return UNRESOLVED;
            } else if (ordinal == REFUSED.ordinal) {
                return REFUSED;
            } else if (ordinal == REFUSED.ordinal) {
                return REFUSED;
            } else if (ordinal == TIMEOUT.ordinal) {
                return TIMEOUT;
            } else {
                throw new IllegalArgumentException("Invalid ordinal.");
            }
                
        }
        
        public byte getCode() {
            return ordinal;
        }
        public String getMsg() {
            return name;
        }
    }
    public static class ConnectionStatus extends BaseOfPseudoEnums {
        /** Now connected. */
        public static final ConnectionStatus CONNECTED = new ConnectionStatus((byte) 1, "Currently connected");
        /** Not connected yet. */
        public static final ConnectionStatus NOTCONNECTED = new ConnectionStatus((byte) 2, "Not connected yet");
        /** Connection failed. Use getError to figure out what went wrong */
        public static final ConnectionStatus FAILED = new ConnectionStatus((byte) 3, "Failed");
        
        public static final byte ORDINAL = 0x02;
        public static final String NAME = "ConnectionStatus";
        
        public final byte ordinal;
        public final String name;
        
        public ConnectionStatus(byte ordinal, String name) {
            super(ordinal, name);
            this.ordinal = ordinal;
            this.name = name;
        }
        public BaseOfPseudoEnums fromCode(byte ordinal) {
            if (ordinal == CONNECTED.ordinal) {
                return CONNECTED;
            } else if (ordinal == NOTCONNECTED.ordinal) {
                return NOTCONNECTED;
            } else if (ordinal == FAILED.ordinal) {
                return FAILED;
            } else {
                throw new IllegalArgumentException("Code does not correspond to a connection status.");
            }
        }
    }
    /**
     * Gets enum from a string.
     * @param <T>
     * @param ordinal A byte representation of an enum.
     * @return 
     */
    public static <T extends BaseOfPseudoEnums> T getEnum(byte ordinal) {
        throw new UnsupportedOperationException("Operation not supported just yet.");
    }
}