package com.gitlab.conicalcat.coniconnect.util;

import com.gitlab.conicalcat.coniconnect.exception.*;
import java.io.*;
import java.net.ServerSocket; import java.net.Socket;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Arrays;

/**
 * @author ConicalCat
 * THE connection class used for transmitting information. Construction does NOT
 * imply connection. Use <code> {@link #connect(String url)} </code>
 * for that purpose.
 */
public class Connection implements Closeable {
    private PseudoEnums.ConnectionStatus status;
    private PseudoEnums.ConnectionError fail;
    private boolean autoLogging = false;
    private boolean listening = true;
    private InputStream inputStream;
    private Thread asyncTransmissionThread;
    private ServerSocket serverSocket;
    private Socket socket;
    
    public Connection(boolean autoLogging) {
        status = PseudoEnums.ConnectionStatus.NOTCONNECTED;
        this.fail = null;
    }
    public PseudoEnums.ConnectionStatus getStatus() {
        return status;
    }
    public PseudoEnums.ConnectionError getFailed() {
        return fail;
    }
    /**
     * Connects to the specified hostname.
     * @param ipAddress The ipAddress to connect to. MUST BE IPv4 in byte array
     * form.
     * @param port The port to connect through.
     * @throws InvalidPortException
     * @throws NetException
     */
    public synchronized void connect(byte[] ipAddress, int port) throws 
            InvalidPortException, NetException {
        if (port > 65535) {
            throw new InvalidPortException("This port number is invalid; "
                    + "port must be within the range 0-65535.");
        }
        byte[] truncIpAddress = Arrays.copyOf(ipAddress, 4);
        try {
            InetAddress address = InetAddress.getByAddress(truncIpAddress);
            socket = new Socket(address, port);
        } catch (UnknownHostException e) {
            fail = PseudoEnums.ConnectionError.UNRESOLVED;
        } catch (IOException e) {
            throw new NetException(e.getMessage());
        } finally {
            if (autoLogging) {
                String ipAsString;
                StringBuilder sb = new StringBuilder();
                for (byte b : truncIpAddress) { 
                    sb.append(b).append(".");
                }
                if(sb.length()>0)
                    sb.setLength(sb.length()-1);
                sb.trimToSize();
                ipAsString = sb.toString();
                
                ConnectionLog.log("Connected to " + ipAsString);
                status = PseudoEnums.ConnectionStatus.CONNECTED;
            }
        }
    }
    public void listen(int port) throws InvalidPortException {
        if (port > 65535) {
            throw new InvalidPortException("This port number is invalid; "
                    + "port must be within the range 0-65535.");
        }
    }
    public void disconnect() throws ConnectionStateException {
        if (asyncTransmissionThread != null) {
            throw new ConnectionStateException("Cannot disconnect while "
                    + "asynchronously transferring data.");
        } else {
            
        }
    }
    /**
     * 
     * @param data The integer data to accept.
     */
    public void asyncTransmission(ArrayList<Integer> data) {
        asyncTransmissionThread = new Thread(() -> {
            for (int part:data) {
                
            }
        });
    }
    public void setAutoLog(boolean autoLogging) {
        
    }
    public void setAutoLog() {
        
    }
    
    @Override
    public void close() throws IOException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}