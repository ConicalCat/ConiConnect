/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.gitlab.conicalcat.coniconnect.util;

import java.io.IOException;
import java.util.logging.FileHandler;
import java.util.logging.Handler;
import java.util.logging.Logger;
import java.util.logging.Level;

/**
 * @author ConicalCat
 * This enum is an easy way to log connections. These methods are called
 * automatically after
 * {@link com.gitlab.conicalcat.coniconnect.util.Connection#SetAutoLog(boolean
 * autoLogging) <code>SetAutoLog</code>} is called with the value
 * <code>true</code>.
 * {@see com.gitlag.conicalcat.coniconnect.util.Connection}
 */
public enum ConnectionLog {
    LOG;
    private static final Logger LOGGER = Logger.getLogger("ConnectionLog");
    private static FileHandler fh;
    /**
     * Logs info asynchronously.
     * @param info the text to be logged, also can print stack traces.
     */
    public static final void logAsync(String info) {
        new Thread(()->{
            LOGGER.info(info);
        }).start();
    }
    /**
     * Logs errors asynchronously.
     * @param errorText the text to be logged, also can print stack traces.
     */
    public static final void errorLogAsync(String errorText) {
        new Thread(()->{
            LOGGER.log(Level.SEVERE, errorText);
        }).start();
    }
    public static final synchronized void log(String info) {
        LOGGER.info(info);
    }
    public static final synchronized void errorLog(String errorText) {
        LOGGER.log(Level.SEVERE, errorText);
    }
    /**
     * Sets the file for log.
     * @param filePath The log file path.
     * @throws java.io.IOException
     */
    public static final synchronized void setFile(String filePath) throws
            IOException {
        if (LOGGER.getHandlers().length > 0) {
            for (Handler handler:LOGGER.getHandlers()) {
                LOGGER.removeHandler(handler);
            }
        }
        fh = new FileHandler("filePath");
        LOGGER.addHandler(fh);
    }
}