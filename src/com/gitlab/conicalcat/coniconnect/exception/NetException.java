/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.gitlab.conicalcat.coniconnect.exception;

import java.io.IOException;

/**
 * @author ConicalCat
 * The mother of all 
 */
public class NetException extends IOException {

    /**
     * Creates a new instance of <code>NetException</code> without detail message.
     */
    public NetException() {
    }


    /**
     * Constructs an instance of <code>NetException</code> with the specified detail message.
     * @param msg the detail message.
     */
    public NetException(String msg) {
        super(msg);
    }
}
