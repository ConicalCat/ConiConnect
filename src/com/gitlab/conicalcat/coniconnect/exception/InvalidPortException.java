/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.gitlab.conicalcat.coniconnect.exception;

/**
 *
 * @author ConicalCat
 */
public class InvalidPortException extends NetException {

    /**
     * Creates a new instance of <code>InvalidPortException</code> without detail message.
     */
    public InvalidPortException() {
    }


    /**
     * Constructs an instance of <code>InvalidPortException</code> with the specified detail message.
     * @param msg the detail message.
     */
    public InvalidPortException(String msg) {
        super(msg);
    }
}
