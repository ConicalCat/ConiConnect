/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.gitlab.conicalcat.coniconnect.exception;

/**
 *
 * @author ConicalCat
 */
public class ConnectionStateException extends NetException {

    /**
     * Creates a new instance of <code>ConnectionStateException</code> without detail message.
     */
    public ConnectionStateException() {
    }


    /**
     * Constructs an instance of <code>ConnectionStateException</code> with the specified detail message.
     * @param msg the detail message.
     */
    public ConnectionStateException(String msg) {
        super(msg);
    }
}
