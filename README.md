# ConiConnect
## The connection library
**ConiConnect** is a connection library that is free to use with credit for non-commercial purposes. HOWEVER, you must consult [Henry Jones](mailto:conicalcatwin@gmail.com) for a quote for commercial purposes.
## The good
This library is updated/edited *regularly* (or will be before EOL), so be sure to check it sometimes and pull changes. Also I promise to keep functions each update that are superceded deprecated and keep their function the same.
## The bad
This library is regularly updated in Java, so recompiling regularly is a requirement! (at least until a possible EOL date) Also, I plan on using JDK 8 features.
## Get it with command line

### In a git repository

#### With HTTPS

    git pull https://gitlab.com/ConicalCat/ConiConnect.git

### Outside of a git repository

You need to do

    git init

in the desired directory first. This will also provide you with a git repository for your project where you want this source code, however you will need to configure it!

## Get it off of Gitlab

Go to [this repository on Gitlab](https://gitlab.com/ConicalCat/ConiConnect).

Thanks, and have fun.